# 20000 Brazilian names

This repository retrieves the nice work from [MedidaSP](https://github.com/MedidaSP/) and sort masculine and feminine names in frequency order. The result is a 10 lowercase wordlists. The data is retrived from [this repository](https://github.com/MedidaSP/nomes-brasileiros-ibge).

## Getting started

This repository just holds the code I used to generate the wordlist and the wordlists can be found in wordlists folder.

## License
The code is GPLv3

