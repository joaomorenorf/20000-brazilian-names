import requests
from io import StringIO
import csv
import os

fem_url = "https://raw.githubusercontent.com/MedidaSP/nomes-brasileiros-ibge/master/ibge-fem-10000.csv"
mas_url = "https://raw.githubusercontent.com/MedidaSP/nomes-brasileiros-ibge/master/ibge-mas-10000.csv"

fem_csv = requests.get(fem_url)
mas_csv = requests.get(mas_url)

fem_list = csv.DictReader(StringIO(fem_csv.content.decode('utf8')))
mas_list = csv.DictReader(StringIO(mas_csv.content.decode('utf8')))


fem_entry = next(fem_list)
mas_entry = next(mas_list)

final_list = []

def select_fields(entry):
    return [entry['nome'], entry['freq']]

def final(comp, list):
    comp['freq'] = int(comp['freq'])
    final_list.append(select_fields(comp))
    for entry in list:
        entry['freq'] = int(entry['freq'])
        final_list.append(select_fields(entry))

while True:
    fem_entry['freq'] = int(fem_entry['freq'])
    mas_entry['freq'] = int(mas_entry['freq'])
    if fem_entry['freq'] > mas_entry['freq']:
        final_list.append(select_fields(fem_entry))
        try:
            fem_entry = next (fem_list)
        except StopIteration:
            final(mas_entry, mas_list)
            break
    elif fem_entry['freq'] < mas_entry['freq']:
        final_list.append(select_fields(mas_entry))
        try:
            mas_entry = next (mas_list)
        except StopIteration:
            final(fem_entry, fem_list)
            break
    elif fem_entry['freq'] == mas_entry['freq']:
        final_list.append(select_fields(fem_entry))
        try:
            fem_entry = next (fem_list)
        except StopIteration:
            final(mas_entry, mas_list)
            break
        final_list.append(select_fields(mas_entry))
        try:
            mas_entry = next (mas_list)
        except StopIteration:
            final(fem_entry, fem_list)
            break

dir="wordlists"
os.mkdir(dir)

i = 0
j = 0
k = 0
for entry in final_list:
    j+=entry[1]
    i+=1
    if ((j >      10**7 and k == 0) or
        (j >  5 * 10**7 and k == 1) or
        (j >  8 * 10**7 and k == 2) or
        (j > 10 * 10**7 and k == 3) or
        (j > 11 * 10**7 and k == 4) or
        (j > 12 * 10**7 and k == 5) or
        (j > 13 * 10**7 and k == 6) or
        (j > 14 * 10**7 and k == 7) or
        (j > 15 * 10**7 and k == 8)):
        with open(dir+"/"+str(k)+"-brazilian-names-"+str(j)+"citizens-"+str(i)+"names.txt",'w') as f:
            for line in final_list[:i]:
                f.write(line[0].lower()+'\n')
        k+=1

with open(dir+"/"+str(k)+"-brazilian-names-"+str(j)+"citizens-"+str(i)+"names.txt",'w') as f:
            for line in final_list[:i]:
                f.write(line[0].lower()+'\n')
